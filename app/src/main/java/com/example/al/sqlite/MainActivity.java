package com.example.al.sqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText name;
    EditText year;
    EditText result;
    DBHelper dbHelper;
    SQLiteDatabase db;
    final String TABLE_NAME = "person";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);

        name = findViewById(R.id.editTextName);
        year = findViewById(R.id.editTextYear);
        result = findViewById(R.id.editTextResult);
        findViewById(R.id.buttonAdd).setOnClickListener(this);
        findViewById(R.id.buttonClear).setOnClickListener(this);
        findViewById(R.id.buttonFill).setOnClickListener(this);

        updateResult();
    }

    public void updateResult() {
        result.setText("");

        db = dbHelper.getWritableDatabase();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            int id = c.getColumnIndex("id");
            int name = c.getColumnIndex("name");
            int year = c.getColumnIndex("year");
            do {
                result.setText(
                        result.getText() + "\n" +
                        "id=" + c.getInt(id) +
                        ", name=" + c.getString(name) +
                        ", year=" + c.getInt(year)
                );
                Log.i("_SQLITE_", "id=" + c.getInt(id) + ", name=" + c.getString(name) + ", year=" + c.getInt(year));
            } while (c.moveToNext());
        } else {
            Log.i("_SQLITE_", "0 records");
        }
        c.close();
        db.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAdd:
                try {
                    String newName = name.getText().toString();
                    if (newName.equals("")) {
                        throw new Exception("invalid name");
                    }
                    int newYear = Integer.parseInt(year.getText().toString());
                    if (0 == newYear) {
                        throw new Exception("invalid year");
                    }
                    ContentValues cv = new ContentValues();
                    cv.put("name", newName);
                    cv.put("year", newYear);
                    db = dbHelper.getWritableDatabase();
                    long index = db.insert(TABLE_NAME, null, cv);
                    Log.i("_SQLITE_", "insert id = " + index);
                    db.close();
                    updateResult();
                } catch (Exception e) {
                    Log.i("_SQLITE_", e.getMessage());
                }
                break;

            case R.id.buttonFill:
                db = dbHelper.getWritableDatabase();
                Cursor c = db.rawQuery(
                        "insert into " + TABLE_NAME + " (name, year) values (\"qqq\", 1111), (\"www\", 2222)",
                        null
                );
                c.getCount(); //does not insert wo getCount()
                c.close();
                db.close();
//                ContentValues cv = new ContentValues();
//                String newName = name.getText().toString();
//                cv.put("name", newName);
//                db.update(TABLE_NAME, cv, "id = ?", new String[] {"2"});
                updateResult();
                break;

            case R.id.buttonClear:
                db = dbHelper.getWritableDatabase();
                int count = db.delete(TABLE_NAME, null, null);
                Log.i("_SQLITE_", "deleted rows count = " + count);
                db.close();
                updateResult();
                break;

        }
    }

    class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context) {
            super(context, "testDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.i("_SQLITE_", "on create");
            db.execSQL(
                    "create table " + TABLE_NAME + " (" +
                    "id integer primary key autoincrement," +
                    "name varchar(100)," +
                    "year integer" +
                    ");"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
